import { Component } from '@angular/core';
import { CommonRequestService } from 'src/services/common-request.service';
import { RestApiService } from 'src/services/rest-api.service';
declare var CryptoJSAesJson: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  secretKey = '123456';
  constructor(
    private commonService: CommonRequestService,
    private restApi: RestApiService
  ) {}

  ReceiveData() {
    this.commonService.showLoading();

    this.restApi.getData('send-payload.php', false).subscribe((response) => {
      console.log(response);

      this.commonService.notice_msg(
        'Encrypted Data Received From API : \n\n' + response
      );

      setTimeout(() => {
        this.commonService.success_msg(
          'Decrypted by App : \n\n' +
            JSON.stringify(CryptoJSAesJson.decrypt(response, this.secretKey))
        );
      }, 4000);
    });
    this.commonService.hideLoading();
  }
}
