import { Component } from '@angular/core';
import { CommonRequestService } from 'src/services/common-request.service';
import { RestApiService } from 'src/services/rest-api.service';
declare var CryptoJSAesJson: any;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  secretKey = '123456';
  planText = {
    Name: 'Mr. Xyz',
    Gender: 'Male',
    Hobby: ['Cricket', 'Football', 'Volleyball'],
  };

  constructor(
    private commonService: CommonRequestService,
    private restApi: RestApiService
  ) {}

  SendData() {
    this.commonService.showLoading();
    const encryptedData = CryptoJSAesJson.encrypt(
      this.planText,
      this.secretKey
    );
    console.log(encryptedData);

    const params = {
      payload: encryptedData,
    };
    const queryString = this.commonService.objectToQuerystring(params);
    this.restApi
      .postData(queryString, 'receive-payload.php', false)
      .subscribe((response) => {
        console.log(response);

        this.commonService.notice_msg(
          'Encrypted by App : \n\n' + encryptedData
        );

        setTimeout(() => {
          this.commonService.success_msg(
            'Decrypted Data Received From API: \n\n' + JSON.stringify(response)
          );
        }, 4000);
      });
    this.commonService.hideLoading();
  }
}
