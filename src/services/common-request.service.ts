import { Injectable } from '@angular/core';
import { throwError, BehaviorSubject } from 'rxjs';
import { formatDate } from '@angular/common';
import { ToastController, LoadingController } from '@ionic/angular';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CommonRequestService {
  baseUrl = 'https://www.emaad-infotech.com/api/dist/';
  deviceOS = this.getDeviceOS();
  loaderToShow: any;
  myDatas: any;

  private profileData = new BehaviorSubject<object>({});
  public getuserData = this.profileData.asObservable();

  constructor(
    public toastController: ToastController,
    public loadingController: LoadingController
  ) {}

  updateUserData(content: any) {
    this.profileData.next(content);
  }

  getAndroidVersionCode() {
    return 1; // 11th Nov, 2020:  increment here before upload to play store
    // keep siteData.android_version same as above for h...
  }

  getiOSVersionCode() {
    return 1; // 11th Nov, 2020:  increment here before upload to app store
    // keep siteData.ios_version same as above for h...
  }

  async error_msg(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000,
      animated: true,
    });
    toast.present();
  }

  async success_msg(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 8000,
      animated: true,
      color: 'dark',
    });
    toast.present();
  }

  async notice_msg(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 4000,
      animated: true,
      color: 'tertiary',
    });
    toast.present();
  }

  showLoading(msg = '', duration = 0) {
    this.loaderToShow = this.loadingController
      .create({
        spinner: 'bubbles',
        duration: duration != 0 ? duration : 5000,
        message: msg != '' ? msg : 'Please Wait...',
        translucent: true,
        cssClass: 'custom-class custom-loading',
        backdropDismiss: false,
      })
      .then((res) => {
        res.present();
      });
  }

  hideLoading() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 1000);
  }

  // Error handling
  handleError(error: {
    error: { message: string };
    status: any;
    message: any;
  }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    this.show_network_err('Network error occured. Please try later.');

    //this.show_network_err(errorMessage);
    return throwError(errorMessage);
  }

  // show network error
  show_network_err(msg: string) {
    alert(msg);
  }

  log(response: any, show = 'Yes') {
    if (show === 'Yes') {
      console.log(response);
    }
  }

  saveSession(key: any, val: any, type = 'local') {
    if (type === 'local') {
      localStorage.setItem(key, val);
    } else {
      sessionStorage.setItem(key, val);
    }
  }

  getSession(key: any, type = 'local') {
    if (type === 'local') {
      return localStorage.getItem(key);
    } else {
      return sessionStorage.getItem(key);
    }
  }

  destroySession(key, type = 'local') {
    if (type === 'local') {
      localStorage.removeItem(key);
    } else {
      sessionStorage.removeItem(key);
    }
  }

  clearAllStorage(type = 'local') {
    if (type === 'local') {
      localStorage.clear();
    } else {
      sessionStorage.clear();
    }
  }

  am_i_online() {
    if (
      typeof this.getSession('user_info') !== 'undefined' &&
      this.getSession('user_info') !== null
    ) {
      return true;
    } else {
      return false;
    }
  }

  getToken() {
    const user = JSON.parse(this.getSession('user_info'));
    if (user !== null) {
      return user.jwt_tocken;
    } else {
      return null;
    }
  }

  getUserId() {
    const user = JSON.parse(this.getSession('user_info'));
    if (user !== null) {
      return user.id;
    } else {
      return null;
    }
  }

  getDeviceOS() {
    const device = JSON.parse(this.getSession('device_info'));
    if (device !== null) {
      return device.platform;
    } else {
      return null;
    }
  }

  getDataType(input) {
    return typeof input;
  }

  getFormattedDate(value, defaultFormat = 'yyyy-MM-dd') {
    return formatDate(value, defaultFormat, 'en');
  }

  checkObjectIsEmpty(obj) {
    return obj === null || undefined
      ? true
      : (() => {
          for (const prop in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, prop)) {
              return false;
            }
          }
          return true;
        })();
  }

  checkArrayIsEmpty(array) {
    if (typeof array !== 'undefined' && array.length > 0) {
      return false;
    } else {
      return true;
    }
  }

  // convert object to formdata query string
  objectToQuerystring(params: {}) {
    return Object.keys(params)
      .map((key) => key + '=' + encodeURIComponent(params[key]))
      .join('&');
  }

  is_numeric(str) {
    return /^\d+$/.test(str);
  }

  indianCurrency(value) {
    if (!isNaN(value)) {
      //  var currencySymbol = '₹';
      var currencySymbol = '';
      if (value == null) {
        return '';
      }
      var InrRSOut = value;
      InrRSOut = Math.round(InrRSOut);
      var RV = '';
      if (InrRSOut > 0 && InrRSOut < 1000) {
        RV = InrRSOut.toString();
      } else if (InrRSOut >= 1000 && InrRSOut < 10000) {
        RV = InrRSOut.toString();
      } else if (InrRSOut >= 10000 && InrRSOut < 100000) {
        var f1 = InrRSOut.toString().substring(0, 2);
        var f2 = InrRSOut.toString().substring(2, 5);
        RV = f1 + ',' + f2;
      } else if (InrRSOut >= 100000 && InrRSOut < 1000000) {
        var f1 = InrRSOut.toString().substring(0, 1);
        var f2 = InrRSOut.toString().substring(1, 3);
        if (f2 == '00') {
          RV = f1 + ' Lacs';
        } else {
          RV = f1 + '.' + f2 + ' Lacs';
        }
      } else if (InrRSOut >= 1000000 && InrRSOut < 10000000) {
        var f1 = InrRSOut.toString().substring(0, 2);
        var f2 = InrRSOut.toString().substring(2, 4);
        if (f2 == '00') {
          RV = f1 + ' Lacs';
        } else {
          RV = f1 + '.' + f2 + ' Lacs';
        }
      } else if (InrRSOut >= 10000000 && InrRSOut < 100000000) {
        var f1 = InrRSOut.toString().substring(0, 1);
        var f2 = InrRSOut.toString().substring(1, 3);
        if (f2 == '00') {
          RV = f1 + ' Cr';
        } else {
          RV = f1 + '.' + f2 + ' Cr';
        }
      } else if (InrRSOut >= 100000000 && InrRSOut < 1000000000) {
        var f1 = InrRSOut.toString().substring(0, 2);
        var f2 = InrRSOut.toString().substring(2, 4);
        if (f2 == '00') {
          RV = f1 + ' Cr';
        } else {
          RV = f1 + '.' + f2 + ' Cr';
        }
      } else if (InrRSOut >= 1000000000 && InrRSOut < 10000000000) {
        var f1 = InrRSOut.toString().substring(0, 3);
        var f2 = InrRSOut.toString().substring(3, 5);
        if (f2 == '00') {
          RV = f1 + ' Cr';
        } else {
          RV = f1 + '.' + f2 + ' Cr';
        }
      } else if (InrRSOut >= 10000000000) {
        var f1 = InrRSOut.toString().substring(0, 4);
        var f2 = InrRSOut.toString().substring(6, 8);
        if (f2 == '00') {
          RV = f1 + ' Cr';
        } else {
          RV = f1 + '.' + f2 + ' Cr';
        }
      } else {
        RV = InrRSOut.toString();
      }
      return currencySymbol + RV;
    }
  }

  nullUndefToBlank(value) {
    return value == null ? '' : value;
  }

  jsonToTypescriptArray(jsonData) {
    let jsonToBeUsed = [];

    for (var type in jsonData) {
      const item: any = {};
      item.key = type;
      item.value = jsonData[type];
      jsonToBeUsed.push(item);
    }
    return jsonToBeUsed;
  }

  strtoArr(myString) {
    let arr = [];
    if (myString != '') {
      arr = myString.split(',');
    }
    return arr;
  }

  public setData(data) {
    this.myDatas = data;
  }

  public getData() {
    return this.myDatas;
  }

  public truncateString(string, maxLength = 50) {
    if (!string) return null;
    if (string.length <= maxLength) return string;
    return `${string.substring(0, maxLength)}...`;
  }
}
