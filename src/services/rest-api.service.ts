import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonRequestService } from 'src/services/common-request.service';
import { Observable } from 'rxjs';
import {
  retry,
  catchError,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

let headers: HttpHeaders = new HttpHeaders();
headers = headers.append(
  'Content-Type',
  'application/x-www-form-urlencoded; charset=UTF-8'
);

@Injectable({
  providedIn: 'root',
})
export class RestApiService {
  cacheObj = {
    commonDataCache: null,
    VerifiedPropertyCache: null,
    siteDataCache: null,
    myProfileCache: null,
    homePageCache: null,
  };

  private baseUrl = this.commonService.baseUrl;

  constructor(
    private http: HttpClient,
    public commonService: CommonRequestService
  ) {}

  appendTocken() {
    // headers = headers.delete('Authorizationtoken'); // remove old entry
    // if (this.commonService.am_i_online()) {
    //   headers = headers.append(
    //     'Authorizationtoken',
    //     this.commonService.getToken()
    //   );
    // }
  }

  postData(queryString: any, API_URL, appendToken = true): Observable<any> {
    if (appendToken) {
      this.appendTocken(); // set Authorization header tocken
    }

    return this.http
      .post(this.baseUrl + API_URL, queryString, { headers })
      .pipe(
        map((data) => data),
        publishReplay(1),
        refCount(),
        retry(3),
        catchError((error) => this.commonService.handleError(error))
      );
  }

  getData(API_URL, Cache_Name): Observable<any> {
    if (!this.cacheObj[Cache_Name]) {
      this.cacheObj[Cache_Name] = this.http.get(this.baseUrl + API_URL).pipe(
        map((data) => data),
        publishReplay(1), // this tells Rx to cache the latest emitted
        refCount(),
        retry(3),
        catchError((error) => this.commonService.handleError(error))
      );
    }
    return this.cacheObj[Cache_Name];
  }
}
